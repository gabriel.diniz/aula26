public class aula26 { // operadores de incremento

    public static void main(String[] args) {

        /*
        int i = 0;
        int y = 0;

        i += 1; // ++i; PRE-incremento
        System.out.println( i );

        System.out.println( y );
        i += 1; // i++; POS-incremento

        // Pre - o valor será incrementado antes da instrução aonde a variavel estiver contida
        // Pos - o valor será incrementado após a execução aonde nossa expressão estiver contida

        int a = 0;
        int b = 0;

        System.out.println(" - - - - - - -");
        System.out.println( --a);  // PRE DECREMENTO

        b--;
        System.out.println( b--);  // POS DECREMENTO
        System.out.println(" - - - - - - -");
         */

        int u = 0;

        u = u + 10; // forma tradicional
        u += 10; // operador de atribuição
        ++u; // operador de incremento

    }
}
